const fs = require('fs'),
	path = require('path'),
	gulp = require('gulp'),
	handlebars = require('gulp-compile-handlebars'),
	rename = require('gulp-rename'),
	slugify = require('slugify'),
	data = require('gulp-data'),
	fileinclude = require('gulp-file-include'),
	prettyHtml = require('gulp-pretty-html'),
	mailFolders = './mail';

function getFolders(dir) {
	return fs.readdirSync(dir)
		.filter((file) => {
			return fs.statSync(path.join(dir, file)).isDirectory();
		});
}


gulp.task('fileinclude', () => {
	const folders = getFolders(mailFolders);
	
	return folders.map((folder) => {
		const confFile = `mail/${folder}/content.json`;
		if(!fs.existsSync(confFile)) {
			gulp.src([`mail/${folder}/template/index.html`]) // index.html ana template
				.pipe(fileinclude({
					prefix: '@',
					basepath: '@file'
				}))
				.pipe(prettyHtml({
					indent_size: 1,
					indent_char: '	',
				}))
				.pipe(gulp.dest(`mail/${folder}/build`));
		}
	})
});

gulp.task('jsoninclude', () => { 

	const folders = getFolders(mailFolders);
	return folders.map((folder) => {
		const confFile = `mail/${folder}/content.json`
		// console.log(confFile)
		// console.log(fs.existsSync(confFile))

		if(fs.existsSync(confFile)) {

			let rawdata = fs.readFileSync(`mail/${folder}/content.json`),
				json = JSON.parse(rawdata);
	
			for (let data of json) {
				const fileName = slugify(data.fullName).toLowerCase();
				gulp.src(`mail/${folder}/template/html.handlebars`)
					.pipe(handlebars(data))
					.pipe(rename(`${fileName}.html`))
					.pipe(gulp.dest(`mail/${folder}/build`));
			}
		}
		
	})
});