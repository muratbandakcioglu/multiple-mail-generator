# HTML Mail templates

Template kullanarak mail HTML oluşturmak için yardımcı uygulama.

## İndirme
Downloads sekmesinden indirebilirsiniz. 

Önemli Not: Bilgisayarınızda Node.js kurulu olması gerekir.

## Kurulum

Klasör konumunda Terminal/Cmd açılır ve aşağıdaki komut girilir.

```
npm install
```

## Kullanım Şekilleri

* JSON kullanarak aynı template ile bir çok HTML üretmek
* Tek template içinde birden fazla html include ederek kullanmak.

### Klasör yapısı
	
	root  
    ├── mail  
        └── Çalışma Dosayası  
        	├── build  (otomatik oluşur)
        	├── template  
			|	├── html.handlebars  (json kullanan yapı için)  
			|	└── index.html  (include kullanan yapı için)  
        	└── content.json (json kullanan yapı için) 
	└── node_modules 

### JSON yardımıyla html üretmek
JSON objeleri içinde dönerek gerekli alanlara içerikleri basarak HTML üretir. Template düzenlendikten ve JSON objeleri oluşturulduktan sonra aşağıdaki komut çalıştırılır. Handlebars blok yapısını kullanır.

```
gulp jsoninclude
```

Çıktı build klasörüne eklenir.

### Birden fazla template kullanarak tek html üretmek

HTML içindeki farklı blokları ayrı ayrı çalışmak için kullanılır. Yapı kurulduktan sonra aşağıdaki komut çalıştırılır

```
gulp fileinclude
```

Çıktı build klasörüne eklenir.

## Linkler
* [Nodejs](https://nodejs.org/en/)
* [Handlebars](https://handlebarsjs.com/)
* [gulp-file-include](https://www.npmjs.com/package/gulp-file-include)
* [gulp-compile-handlebars](https://www.npmjs.com/package/gulp-compile-handlebars)
